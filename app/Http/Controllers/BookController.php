<?php
    namespace App\Http\Controllers;

    use App\Book;
    use App\Http\Requests;
    use App\Http\Controllers\Controller;
    use Request;
    use View;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Validator;

    class BookController extends Controller {

        public function index()
        {
            $books=Book::all();
            return view('books.index',compact('books'));
        }

        public function create()
        {
            return view::make('books.create');
        }


        public function store()
        {
            $book=input::all();
            Book::create($book);
            return redirect('books');
        }

        public function show($id)
        {
            $book=Book::find($id);
            return view('books.show',compact('book'));
        }
        public function edit($id)
        {
            $book=Book::find($id);
            return view('books.edit',compact('book'));
        }


        public function update($id)
        {
            //
            $bookUpdate=Request::all();
            $book=Book::find($id);
            $book->update($bookUpdate);
            return redirect('books');
        }
        public function destroy($id)
        {
            Book::find($id)->delete();
            return redirect('books');
        }
    }